<?php
define("PATH","/var/www/html/sistema-biblioteca-mvc/");
define("siteURL","http://localhost/sistema-biblioteca-mvc/");

function error($e,$confirm){
    $confirm=isset($confirm)?"\"$confirm\"":"\"true\"";
    ?>
    <script>
        $(document).ready(function () {
            sweetAlertInitialize();
            swal({title: "",
                text: "<?= $e;?>",
                showConfirmButton: <?= $confirm;?>,
                html: true,
                confirmButtonClass: "btn-danger",
                type: 'error',
                confirmButtonText: 'Fechar'});
        });
    </script>
    <?php
}
function success($e,$confirm){
    $confirm=isset($confirm)?"\"$confirm\"":"\"true\"";
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            sweetAlertInitialize();
            swal({title: "",
                text: "<?= $e;?>",
                showConfirmButton: <?= $confirm;?>,
                html: true,
                type: 'success',
                confirmButtonClass: "btn-success",
                confirmButtonText: 'Fechar'});
        })
    </script>
    <?php
}
function question($e,$function){
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            sweetAlertInitialize();
            swal({
                    title: "",
                    text: "<?= $e;?>",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-warning",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Nao",
                    closeOnConfirm: false
                },
                <?= $function;?>);
        })
    </script>
    <?php
}
function printR($array){
   echo  "<pre>";print_r($array);echo "</pre>";
}
function updateTitle($title){
    echo /** @lang HTML */'<script type="text/javascript">$("title").html("'.$title.' || Biblioteca");</script>';
}
function setTitle($title){
    echo /** @lang HTML */'<title>'.$title.' || Biblioteca</title>';

}
function jsonDefault($success,$msgError){
    return json_encode(array("success" => $success,"msg" => $msgError));
}