<?php

/**
 * Created by PhpStorm.
 * User: jorge-jr
 * Date: 25/03/16
 * Time: 08:56
 */
require_once PATH."model/class.categoria.php";
class livro extends categoria
{
    public function cadastrarLivro($nome,$capa,$qtd,$categoria,$descricao){
        $descricao=addslashes($descricao);
        $nome=addslashes($nome);
        $capa=addslashes($capa);
        $qtd=(int)$qtd;
        $this->connect();
        $this->query("INSERT INTO ".$this->prefixo."livros(nome,capa,quantidade,id_categoria,descricao) VALUES('$nome','$capa','$qtd',$categoria,'$descricao')");
        $ar=$this->affected_rows;
        $this->close();
        if($ar==1){
            return true;
        }
        else{
            return false;
        }
    }
    public function getAllLivros(){
        $this->connect();
        $res=$this->query("SELECT l.id_livro,l.nome,l.capa,l.quantidade,c.nome as categoria FROM ".$this->prefixo."livros l,".$this->prefixo."categorias_livros c WHERE c.id_categoria=l.id_categoria");
        $this->close();
        return $this->fetch_all($res,'assoc');
    }
    public function getLivroData($livroID){
        $this->connect();
        $res=$this->query("SELECT l.id_livro,l.nome,l.capa,l.quantidade,c.id_categoria as categoria,l.descricao FROM ".$this->prefixo."livros l,".$this->prefixo."categorias_livros c WHERE c.id_categoria=l.id_categoria AND l.id_livro=$livroID");
        $this->close();
        return $res->fetch_assoc();
    }
}