<?php

/**
 * Created by PhpStorm.
 * User: jorge-jr
 * Date: 20/03/16
 * Time: 20:06
 */
require_once PATH."model/class.conexao.php";
class aluno extends conexao
{
    public function getAllAlunos(){
        $this->connect();
        $res=$this->query("SELECT a.id_aluno,a.nome,a.cpf FROM ".$this->prefixo."aluno a");
        $this->close();
        return $res->fetch_all(MYSQLI_ASSOC);
    }
    public function getAlunoData($id){
        $this->connect();
        $res=$this->query("SELECT a.id_aluno,a.nome,a.cpf FROM ".$this->prefixo."aluno a WHERE a.id_aluno=$id");
        $this->close();
        return $res->fetch_assoc();
    }
    public function alunoExists($id){
        $this->connect();
        $res=$this->query("SELECT id_aluno FROM ".$this->prefixo."aluno WHERE id_aluno=$id");
        $this->close();
        return $res->num_rows;
    }
    public function alunoExistsCPF($cpf){
        $this->connect();
        $res=$this->query("SELECT id_aluno FROM ".$this->prefixo."aluno WHERE cpf like '$cpf'");
        $this->close();
        if($res->num_rows>0){
            return true;
        }
        else{
            return false;
        }
    }
    public function cadastrarAluno($nome,$cpf){
        $nome=addslashes(ucwords(strtolower($nome)));
        $this->connect();
        $this->query("INSERT INTO ".$this->prefixo."aluno(nome,cpf) VALUES('$nome','$cpf')");
        $ar=$this->affected_rows;
        $this->close();
        if($ar>0){
            return true;
        }
        else{
            return false;
        }
    }
    public function getEmprestimosFromAluno($idAluno){
        $this->connect();
        $res=$this->query("SELECT DATE_FORMAT(e.data_emprestimo,'%d/%m/%Y') as data_emprestimo,e.id_emprestimo,l.nome,l.capa,l.id_livro,es.nome as status FROM ".$this->prefixo."emprestimo e,".$this->prefixo."aluno a,".$this->prefixo."livros l,".$this->prefixo."emprestimo_status es WHERE l.id_livro=e.id_livro AND a.id_aluno=e.id_aluno AND a.id_aluno=$idAluno AND es.status=e.status");
        $this->close();
        return $this->fetch_all($res,'assoc');
    }
}