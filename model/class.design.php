<?php

/**
 * Created by PhpStorm.
 * User: jorge-jr
 * Date: 20/03/16
 * Time: 15:46
 */
class design
{
    public function headers($frameworksNeeded){
        global $PARAMS;
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="<?= siteURL;?>headers/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?= siteURL;?>headers/nicescroll/jquery.nicescroll.min.js"></script>
        <script type="text/javascript">
            var origin={
                "siteURL": "<?= siteURL;?>",
                "getSiteURL": function () {
                    return "<?= siteURL;?>";
                },
                "params": <?= json_encode($PARAMS);?>
            }
        </script>
        <?
        if(in_array('bootstrap',$frameworksNeeded)){
            echo "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\"><script type=\"text/javascript\" src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js\"></script><link href=\"".siteURL."headers/metisMenu/metisMenu.min.css\" rel=\"stylesheet\"><link href=\"".siteURL."headers/origin/css/sb-admin-2.css\" rel=\"stylesheet\"><script src=\"".siteURL."headers/metisMenu/metisMenu.min.js\"></script>";
        }
        if(in_array('select2',$frameworksNeeded)){
            echo "<link rel=\"stylesheet\" href=\"".siteURL."headers/select2/css/select2.min.css\"><script type=\"text/javascript\" src=\"".siteURL."headers/select2/js/select2.min.js\"></script><script type=\"text/javascript\" src=\"".siteURL."headers/select2/js/language/pt-BR.js\"></script>";
        }
        if(in_array('datatables',$frameworksNeeded)){
            echo "<link rel=\"stylesheet\" href=\"".siteURL."headers/datatables/dataTables.bootstrap.min.css\"><script type=\"text/javascript\" src=\"".siteURL."headers/datatables/jquery.dataTables.min.js\"></script>
        <script type=\"text/javascript\" src=\"".siteURL."headers/datatables/dataTables.bootstrap.min.js\"></script>";
        }
        if(in_array('font-awesome',$frameworksNeeded)){
            echo "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css\">";
        }
        if(in_array('sweetalert',$frameworksNeeded)){
            echo "<link rel=\"stylesheet\" href=\"".siteURL."headers/sweetalert/sweetalert.css\"><script type=\"text/javascript\" src=\"".siteURL."headers/sweetalert/sweetalert.min.js\"></script>";
        }
        if(in_array('jquery-mask',$frameworksNeeded)){
            echo "<script type=\"text/javascript\" src=\"".siteURL."headers/jquery/jquery-mask.js\"></script>";
        }
        if(file_exists(PATH."view/headers/origin/".$PARAMS[0].DIRECTORY_SEPARATOR.$PARAMS[1].".js")){
            echo "<script type='text/javascript' src='".siteURL."headers/origin/".$PARAMS[0].DIRECTORY_SEPARATOR.$PARAMS[1].".js'></script>";
        }
        if(file_exists(PATH."view/headers/origin/".$PARAMS[0].DIRECTORY_SEPARATOR.$PARAMS[1].".css")){
            echo "<link rel='stylesheet' href='".siteURL."headers/origin/".$PARAMS[0].DIRECTORY_SEPARATOR.$PARAMS[1].".css' ".PATH."view/headers/origin/".$PARAMS[0].DIRECTORY_SEPARATOR.$PARAMS[1].".css".">";
        }

        echo "<script type=\"text/javascript\" src=\"".siteURL."headers/origin/js/script.js\"></script>";
        echo "<link rel='stylesheet' href='".siteURL."headers/origin/css/style.css'>";
    }

}