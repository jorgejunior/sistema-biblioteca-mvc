<?php

/**
 * Created by PhpStorm.
 * User: jorge-jr
 * Date: 25/03/16
 * Time: 09:27
 */
require_once PATH."model/class.conexao.php";
class categoria extends conexao
{
    public function cadastrarCategoria($nome){
        $this->connect();
        $nome=addslashes($nome);
        $this->query("INSERT INTO ".$this->prefixo."categorias_livros(nome) VALUES('$nome')");
        $ar=$this->affected_rows;
        $this->close();
        if($ar==1){
            return true;
        }
        else{
            return false;
        }
    }
    public function getAllCategorias(){
        $this->connect();
        $res=$this->query("SELECT * FROM ".$this->prefixo."categorias_livros WHERE 1");
        $this->close();
        return $this->fetch_all($res,'assoc');
    }
    public function categoriaExists($idCategoria){
        $this->connect();
        $res=$this->query("SELECT id_categoria FROM ".$this->prefixo."categorias_livros WHERE id_categoria=$idCategoria");
        $this->close();
        if($res->num_rows==1){
            return true;
        }
        else{
            return false;
        }
    }
}