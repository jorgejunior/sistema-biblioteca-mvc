<?php
require_once PATH."model/class.livro.php";
$lClass=new livro();
if(!$lClass->alunoExists($_POST['id_aluno'])) {
    die(jsonDefault(false,"Aluno não existe"));
}

echo json_encode(array("success" => true,"emprestimos"=> json_encode($lClass->getEmprestimosFromAluno($_POST['id_aluno']))));