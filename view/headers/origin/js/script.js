$(function() {

    $('#side-menu').metisMenu();

});
$(function() {
    $('html').niceScroll({
        autohidemode: false,
        cursorborderradius: '0px',
        cursorwidth: '8px',
        cursorcolor: '#158cba',
        scrollspeed: "20",
        background: '#FFF',
        cursorborder: "0"
    });
    $(window).bind("load resize", function() {

        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", parseFloat($(window).height())-parseFloat($('#wrapper').css('margin-top')));
        }
    });

});
$(document).ready(function () {
    $('.tt').tooltip();
    $('a').not('.full-link').each(function () {
        link=$(this).attr('href')===undefined?'':$(this).attr('href');
        $(this).attr('href',origin.siteURL+link);
    });
    $('#wrapper').first().css({"margin-top": $('div.navbar-header').height()+$('.navbar-top-links').height()+10});
});
function successSweet(msg,confirm){
    confirm=confirm===undefined?'true':confirm;
            sweetAlertInitialize();
            swal({title: "",
                text: msg,
                showConfirmButton: confirm,
                html: true,
                type: 'success',
                confirmButtonClass: "btn-success",
                confirmButtonText: 'Fechar'});
}
function errorSweet(msg,confirm){
    confirm=confirm===undefined?'true':confirm;
sweetAlertInitialize();
            swal({title: "",
                text: msg,
                showConfirmButton: confirm,
                html: true,
                type: 'error',
                confirmButtonClass: "btn-danger",
                confirmButtonText: 'Fechar'});
}
function questionSweet(msg,confirm,functionToExec){
    confirm=confirm===undefined?'true':confirm;
    sweetAlertInitialize();
            swal({
                    title: "",
                    text: msg,
                    type: "warning",
                    showCancelButton: confirm,
                    confirmButtonClass: "btn-warning",
                    confirmButtonText: "Sim",
                    cancelButtonText: "Nao",
                    closeOnConfirm: false
                },
                function () {
                    var fnstring = functionToExec;
                    var fn = window[fnstring];
                    if (typeof fn === "function") fn();
                });
}