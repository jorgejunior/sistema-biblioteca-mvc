$(document).ready(function(){
    $.getJSON(origin.siteURL+'jsonData/getAllAlunos', function (data) {
        $.each(data, function () {
            aluno=this;
            $('.table-alunos').DataTable().rows.add(
                [{
                "0": aluno.id_aluno,
                    "1": aluno.nome,
                    "2": aluno.cpf,
                    "3": '<a class="fa fa-file fa-lg tt" title="Ver emprestimos" href="'+origin.siteURL+'admin/ver.emprestimos.do.aluno/'+aluno.id_aluno+'"></a> <a class="fa fa-edit fa-lg tt" title="Editar dados do aluno" href="'+origin.siteURL+'admin/editar.aluno/'+aluno.id_aluno+'"></a>'
                }]).draw();
        });
        $('.tt').tooltip();
    });
});