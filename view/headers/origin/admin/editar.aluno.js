$(document).ready(function(){
    $('.btn-reset-form').click(function(){
        questionSweet('Deseja realmente resetar o formulário ?',true,'limparFormulario');
    });
    $('[name=cpf]').mask('000.000.000-00',{"placeholder": '000.000.000-00'});
    $.post(origin.siteURL+"jsonData/getAlunoData",{"id": origin.params[2]},function (data) {
        if(jQuery.isEmptyObject(data)){
            $('[name=formEditarAluno]').remove();
            errorSweet("Aluno nao encontrado.");
        }
        else {
            $('[name=nome]').attr('value', data.nome);
            $('[name=cpf]').attr('value', data.cpf);
        }
    });
});
function limparFormulario(){
    $('[name=formEditarAluno]').each(function () {
        this.reset();
    });
    successSweet('Formulário resetado com sucesso.');
}