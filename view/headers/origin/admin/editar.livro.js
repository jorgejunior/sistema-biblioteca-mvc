$(document).ready(function(){
    $('.btn-reset-form').click(function(){
        questionSweet('Deseja realmente resetar o formulário ?',true,'limparFormulario');
    });
    $.getJSON(origin.siteURL + 'jsonData/getAllCategorias', function (data) {
        $.each(data, function () {
            $('[name=categoria]').append("<option value='" + this.id_categoria + "'>" + this.nome + "</option>");
        });
        $.post(origin.siteURL+"jsonData/getLivroData",{"id": origin.params[2]},function (data) {
            if(jQuery.isEmptyObject(data)){
                $('[name=formEditarLivro]').remove();
                errorSweet("Livro nao encontrado.");
            }
            else {
                $('[name=nome]').attr('value', data.nome);
                $('[name=categoria]').val(data.categoria);
                $('[name=quantidade]').attr('value', data.quantidade);
                $('[name=descricao]').html(data.descricao);
                $('.link-to-ex-image').attr('href','data:image/png;base64,'+data.capa);
            }
        });
    });

});
function limparFormulario(){
    $('[name=formEditarLivro]').each(function () {
        this.reset();
    });
    successSweet('Formulário resetado com sucesso.');
}