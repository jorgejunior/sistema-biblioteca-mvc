$(document).ready(function(){
    $.getJSON(origin.siteURL+'jsonData/getAllLivros', function (data) {
        $.each(data, function () {
            livro=this;
            $('.table-livros').DataTable().rows.add(
                [{
                    "0": livro.id_livro,
                    "1": "<img src='data:image/png;base64,"+livro.capa+"' class='img-rounded img-responsive img-thumbnail img-on-table'>",
                    "2": livro.nome,
                    "3": livro.categoria,
                    "4": '<a class="fa fa-file fa-lg tt" title="Ver emprestimos" href="'+origin.siteURL+'admin/emprestimo.livro/'+livro.id_livro+'"></a> <a class="fa fa-edit fa-lg tt" title="Editar dados do livro" href="'+origin.siteURL+'admin/editar.livro/'+livro.id_livro+'"></a>'
                }]).draw();
        });
        $('.tt').tooltip();
    });
});