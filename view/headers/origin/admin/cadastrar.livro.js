$(document).ready(function() {
    $('.btn-reset-form').click(function () {
        questionSweet('Deseja realmente resetar o formulário ?', true, 'limparFormulario');
    });
    $.getJSON(origin.siteURL + 'jsonData/getAllCategorias', function (data) {
        $.each(data, function () {
            $('[name=categoria]').append("<option value='" + this.id_categoria + "'>" + this.nome + "</option>");
        });
    });
    $('.btn-cadastrar-livro').click(function () {
        questionSweet("Deseja realmente cadastrar esse livro ?",true,'cadastrarLivro');
    })
});
function limparFormulario(){
    $('[name=formCadastrarLivro]').each(function () {
        this.reset();
    });
    successSweet('Formulário resetado com sucesso.');
}
function cadastrarLivro(){
    $('.btn-cadastrar-livro').button('loading');
    var formData = new FormData($('[name=formCadastrarLivro]')[0]);
    $.ajax({
        url: origin.siteURL+'controller/cadastrar.livro',  //Server script to process data
        type: 'POST',
        data: formData,

        xhr: function () {  // Custom XMLHttpRequest
            var myXhr = $.ajaxSettings.xhr();
            myXhr.upload;
            return myXhr;
        },
        cache: false,
        contentType: false,
        processData: false
    }).done(function (data) {
        if(data.success==true){
            successSweet(data.msg);
            $('[name=formCadastrarLivro]').each(function () {
                this.reset();
            });
        }
        else{
            errorSweet(data.msg);
        }
        $('.btn-cadastrar-livro').button('reset');
    });
}