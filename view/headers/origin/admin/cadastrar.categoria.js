$(document).ready(function(){
    $('.btn-reset-form').click(function(){
        questionSweet('Deseja realmente resetar o formulário ?',true,'limparFormulario');
    });
    $('.btn-cadastrar-categoria').click(function () {
        questionSweet("Deseja realmente cadastrar essa categoria ?",true,'cadastrarCategoria');
    })
});
function limparFormulario(){
    $('[name=formCadastrarCategoria]').each(function () {
        this.reset();
    });
    successSweet('Formulário resetado com sucesso.');
}
function cadastrarCategoria(){
    $('.btn-cadastrar-categoria').button('loading');
    $.post(origin.siteURL+"controller/cadastrar.categoria",$('[name=formCadastrarCategoria]').serialize(), function (data) {
        if(data.success==true){
            successSweet(data.msg,true);
            $('[name=formCadastrarCategoria]').each(function () {
                this.reset();
            });
        }
        else{
            errorSweet(data.msg,true);
        }
        $('.btn-cadastrar-categoria').button('reset');
    })
}