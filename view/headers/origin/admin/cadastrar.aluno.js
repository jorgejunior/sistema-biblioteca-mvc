$(document).ready(function(){
    $('.btn-reset-form').click(function(){
        questionSweet('Deseja realmente resetar o formulário ?',true,'limparFormulario');
    });
    $('[name=cpf]').mask('000.000.000-00',{"placeholder": '000.000.000-00'});
    $('.btn-cadastrar-aluno').click(function () {
        questionSweet("Deseja realmente cadastrar o aluno com esses dados ?",true,'cadastrarAluno');

    })
});
function limparFormulario(){
    $('[name=formCadastrarAluno]').each(function () {
        this.reset();
    });
    successSweet('Formulário resetado com sucesso.');
}
function cadastrarAluno(){
    $('.btn-cadastrar-aluno').button('loading');
    $.post(origin.siteURL+"controller/cadastrar.aluno",$('[name=formCadastrarAluno]').serialize(), function (data) {
        if(data.success==true){
            successSweet(data.msg,true);
            $('[name=formCadastrarAluno]').each(function () {
                this.reset();
            });
        }
        else{
            errorSweet(data.msg,true);
        }
        $('.btn-cadastrar-aluno').button('reset');
    })
}