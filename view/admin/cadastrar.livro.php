<? updateTitle("Cadastrar livro");?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <form onsubmit="return false" name="formCadastrarLivro" class="form-horizontal" enctype="multipart/form-data">
            <div class="panel panel-primary">
                <div class="panel-heading">Cadastrar livro</div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="col-md-3"><label for="nome" class="control-label">Nome</label></div>
                        <div class="col-md-9"><input type="text" id="nome" class="form-control" name="nome" placeholder="Digite o nome"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"><label for="cpf" class="control-label">Categoria</label></div>
                        <div class="col-md-9">
                            <select name="categoria" class="form-control"></select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"><label for="capa" class="control-label">Capa</label></div>
                        <div class="col-md-9"><input type="file" id="capa" class="form-control" name="capa"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"><label for="quantidade" class="control-label">Quantidade</label></div>
                        <div class="col-md-9"><input type="number" id="quantidade" class="form-control" name="quantidade" value="0"></div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-3"><label for="descricao" class="control-label">Descrição</label></div>
                        <div class="col-md-9"><textarea id="descricao" class="form-control" name="descricao" rows="10"></textarea></div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button class="btn btn-danger btn-reset-form">Resetar</button>
                    <button class="btn btn-success btn-cadastrar-livro">Cadastrar livro</button>
                </div>
            </div>
        </form>
    </div>
</div>