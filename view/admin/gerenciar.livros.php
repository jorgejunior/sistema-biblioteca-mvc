<? updateTitle("Gerenciar livros");?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="table-responsive">
            <table class="table dataTable table-livros table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Capa</th>
                    <th>Nome</th>
                    <th>Categoria</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </div>
</div>