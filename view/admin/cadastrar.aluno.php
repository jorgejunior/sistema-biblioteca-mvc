<? updateTitle("Cadastrar aluno");?>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <form onsubmit="return false" name="formCadastrarAluno" class="form-horizontal">
        <div class="panel panel-primary">
            <div class="panel-heading">Cadastrar aluno</div>
        <div class="panel-body">
            <div class="col-md-12">
                <div class="col-md-3"><label for="nome" class="control-label">Nome</label></div>
                <div class="col-md-9"><input type="text" id="nome" class="form-control" name="nome" placeholder="Digite o nome"></div>
            </div>
            <div class="col-md-12">
                <div class="col-md-3"><label for="cpf" class="control-label">CPF</label></div>
                <div class="col-md-9"><input type="text" name="cpf" id="cpf" class="form-control"></div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button class="btn btn-danger btn-reset-form">Resetar</button>
            <button class="btn btn-success btn-cadastrar-aluno">Cadastrar</button>
        </div>
        </div>
        </form>
    </div>
</div>