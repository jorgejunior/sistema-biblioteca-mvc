<?php
header("Content-type: application/json");
if(empty($_POST['nome'])){
   die(json_encode(array("success"=> false,"msg"=> "O nome não foi preenchido")));
}
if(strlen($_POST['cpf'])!=14){
    die(json_encode(array("success"=> false,"msg"=> "O CPF não foi preenchido corretamente")));
}
require_once PATH."includes/functions/cpfCheck.php";
if(!cpfCheck($_POST['cpf'],14)){
    die(json_encode(array("success"=> false,"msg"=> "CPF inválido")));
}
require_once PATH."model/class.aluno.php";
$aClass=new aluno();
if($aClass->alunoExistsCPF($_POST['cpf'])){
    die(json_encode(array("success"=> false,"msg"=> "Este CPF já encontra-se em um dos nossos cadastros.")));
}
if(!$aClass->cadastrarAluno($_POST['nome'],$_POST['cpf'])){
    die(json_encode(array("success"=> false,"msg"=> "Houve um erro ao cadastrar o aluno")));
}
else{
    echo  json_encode(array("success" => true,"msg"=> "Aluno cadastrado com sucesso"));
}
