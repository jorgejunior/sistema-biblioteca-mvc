<?php
header("Content-type: application/json");
if(empty($_POST['nome'])){
    die(json_encode(array("success"=> false,"msg"=> "O nome não foi preenchido")));
}
require_once PATH."model/class.categoria.php";
$cClass=new categoria();
if($cClass->cadastrarCategoria($_POST['nome'])) {
    echo jsonDefault(true,"Categoria cadastrada com sucesso.");
}
else{
    die(jsonDefault(false,"Falha ao efetuar o cadastro da categoria"));
}
