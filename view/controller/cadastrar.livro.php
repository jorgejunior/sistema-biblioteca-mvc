<?php
header("Content-type: application/json");
if(empty($_POST['nome'])){
    die(jsonDefault(false,"O nome não foi preenchido."));
}
if(empty($_FILES['capa']['name'])){
    die(jsonDefault(false,"A capa não foi selecionada."));
}
if(!in_array($_FILES['capa']['type'],array("image/jpeg", "image/gif", "image/png"))){
    die(jsonDefault(false,"Só é possível enviar JPEG,GIF e PNG."));
}
if(!$_POST['quantidade']>0 OR empty($_POST['quantidade'])){
    die(jsonDefault(false,"Quantidade inválida."));
}
if(empty($_POST['descricao'])){
    die(jsonDefault(false,"A descrição não foi preenchida."));
}
require_once PATH."model/class.livro.php";
$lClass=new livro();
if(!$lClass->categoriaExists($_POST['categoria'])){
    die(jsonDefault(false,"Esta categoria não existe."));

}
if($lClass->cadastrarLivro($_POST['nome'],base64_encode(file_get_contents($_FILES['capa']['tmp_name'])),$_POST['qtd'],$_POST['categoria'],$_POST['descricao'])){
    echo jsonDefault(true,"Livro cadastrado com sucesso");
}
else{
    die(jsonDefault(false,"Houve um erro ao cadastrar o livro."));
}